from avgstepsgivenmonth import get_month
import statistics



def step_lists(month):

    infile = open('steps.txt', 'r')
    steps = infile.readlines()
    infile.close()
    index = 0
    while index < len(steps):
        steps[index] = int(steps[index])
        index += 1

    jan = steps[0:31]
    feb = steps[31:59]
    mar = steps[59:90]
    apr = steps[90:120]
    may = steps[120:151]
    jun = steps[151:181]
    jul = steps[181:212]
    aug = steps[212:243]
    sep = steps[243:273]
    oct = steps[273:304]
    nov = steps[304:334]
    dec = steps[334:365]
    print(jan)
    x = statistics.mean(sum([block]))

    return step_lists(print('The average distance during ', month, 'was', x))
