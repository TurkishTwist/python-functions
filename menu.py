def menu():

    print("\n")
    print('\t ------MENU------ \t')
    print('1) Average steps monthly.')
    print('2) Average steps in a given month.')
    print('3) Highest number of steps in a month.')
    print('4) Highest numbers of steps in a given month.')
    print('5) Lowest number of steps in a month.')
    print('6) Lowest number of steps in a given month.')
    print('7) View steps for a given month.')
    print('8) Quit.')


