from avgstepsmonth import avg_steps_month
from avgstepsgivenmonth import get_month
from menu import menu
from list import step_lists
from statistics import mean
jan = 1

def main():

    menu()
    user_input = -1
    user_input = (int(input('Pick a menu option: ')))

    while user_input != 0:
        if user_input == 1:
            avg_steps_month()
            main()
        if user_input == 2:
            block = (str(input('Select a month: ')))
            get_month(block)
            step_lists(month)
            main()
        if user_input == 3:
            'Highest numbers of steps overall in a month'
        if user_input == 4:
            'Highest number of steps in a given month'
        if user_input == 5:
            'Lowest number of steps overall in a month'
        if user_input == 6:
            'Lowest number of steps overall in a month'
        if user_input == 7:
            'Print steps for month chosen'
        else:
            'close files'


main()
